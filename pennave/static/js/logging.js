/*
 * $LastChangedDate: 2008-09-01 18:16:28 +0000 (Mon, 01 Sep 2008) $
 * $LastChangedRevision: 45 $
 * $LastChangedBy: a.skwar $
 * $HeadURL: http://pas3.googlecode.com/svn/trunk/pennave/static/js/logging.js $
 * $Id: logging.js 45 2008-09-01 18:16:28Z a.skwar $
 */

var Logger = Class.create({
    initialize: function(divId) {
        this.log = [];
        this.level = 0;
        if (divId == null) divId = "logging_window";
        this.divId = divId;
    },
    setLevel: function(level) {
        this.level = level;
        alert("set level!");
    },
    l: function(message, level) { this.logMessage(message, level); },
    logMessage: function(message, level) {
        this.log.push(new Array(message, level));
        if (level == null) { level = 0; }
        if (level >= this.level) {
            var spanNode = document.createElement("span");
            $(spanNode).appendChild(document.createTextNode(message));
            $(spanNode).addClassName("log"+level);
            $(this.divId).appendChild(spanNode);
        }
    },
    DEBUG: 0,
    INFO: 10,
    WARN: 20,
    WARNING: 20,
    ERROR: 30,
    CRITICAL: 40,
    FATAL: 50,
    FAIL: 50
});

var logger = new Logger();

#===============================================================================
# vim: set fileencoding=utf-8
# 
# $LastChangedDate: 2008-09-01 18:16:28 +0000 (Mon, 01 Sep 2008) $
# $LastChangedRevision: 45 $
# $LastChangedBy: a.skwar $
# $HeadURL: http://pas3.googlecode.com/svn/trunk/pennave/tests/support.py $
# $Id: support.py 45 2008-09-01 18:16:28Z a.skwar $
#===============================================================================

# support.py
#
# this module contains general support scripts that help create a test
# database harness.  Mainly, it creates a sample database and some images.
#

import sys
import os
sys.path.append(".." + os.sep + "src")
from dbobjects import *
import Image
import time
import tempfile

def createTables():
    """Creates the tables in the database"""
    for x in [Photo, PhotoVersion, Tag]: x.createTable()
    
def createTags():
    for x in xrange(1,10):
        t = Tag(name="Tag%d" % x)

def createPhotos():
    for x in xrange(1,10):
        # create the dummy file for the filenae
        fd, fn = tempfile.mkstemp(suffix=".jpg")
        fdir, fname = os.path.split(fn)
        p = Photo(time=time.time(), directoryPath=fdir, name=fname)
        # add the random tags to the collection
        for y in xrange(1,x+1):
            t = Tag.byName("Tag%d" % x)
            p.addTag(t)
            
# this creates a dummy database and some silly image files
def createDummyDatabase():
    databaseURI = "sql:/:memory:"
    conn = connect(databaseURI, debug=False, cache=None, process=None)
    createTables()
    createTags()
    
def cleanupDummyDatabase():
    pass

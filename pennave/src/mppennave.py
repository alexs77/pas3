#===============================================================================
# vim: set fileencoding=utf-8
# 
# $LastChangedDate: 2008-09-01 18:16:28 +0000 (Mon, 01 Sep 2008) $
# $LastChangedRevision: 45 $
# $LastChangedBy: a.skwar $
# $HeadURL: http://pas3.googlecode.com/svn/trunk/pennave/src/mppennave.py $
# $Id: mppennave.py 45 2008-09-01 18:16:28Z a.skwar $
#===============================================================================

# mycpapp.py
from cherrypy import _cpmodpy
import cherrypy

_isSetUp = False
def mphandler(req):
    global _isSetUp
    if not _isSetUp:
        # load_conf_file(req.get_options().get['pennave.config_file'])
        cherrypy.config.update(req.get_options()['pennave.config_file'])
        cherrypy.config.update({"modpython_prefix": req.get_options()['pennave.prefix']})
        cherrypy.config.update({"modpython_server": req.get_options()['pennave.server']})
        _isSetUp = True
    return _cpmodpy.handler(req)
